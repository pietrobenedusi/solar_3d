#ifndef TEST_RII_INCLUDE_HPP
#define TEST_RII_INCLUDE_HPP

#include "RT_solver.hpp"

namespace test_solar_3D {
    int test_rii_3D_include(int argc, char *argv[]);
}

#endif // TEST_RII_INCLUDE_HPP
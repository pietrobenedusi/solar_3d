% Function to convert centimeters to angstroms
function angstrom = cm_to_angstrom(cm)
    angstrom = cm .* 1e8;
end
